{{- define "exapi.vars" -}}
[
  # From Values:
  {
    name: PYTHONUNBUFFERED,
    value: '{{ .Values.env.pythonUnbuffered }}'
  },
  {
    name: CRYPTOBAL_URL,
    value: '{{ .Values.env.cryptobalUrl }}'
  },
  {
    name: SQLALCHEMY_DATABASE_URI,
    value: '{{ .Values.env.sqlAlchemyDatabaseUri }}'
  },
  {
    name: PORT,
    value: '{{ .Values.service.port }}'
  },
]
{{- end -}}