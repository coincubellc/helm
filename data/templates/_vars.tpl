{{- define "data.vars" -}}
[
  # From Values:
  {
    name: PYTHONUNBUFFERED,
    value: '{{ .Values.env.pythonUnbuffered }}'
  },
  {
    name: SQLALCHEMY_DATABASE_URI,
    value: '{{ .Values.env.sqlAlchemyDatabaseUri }}'
  },
  {
    name: CELERY_BROKER_URL,
    value: '{{ .Values.env.celeryBrokerUrl }}'
  },
  {
    name: EXAPI_URL,
    value: '{{ .Values.env.exapiUrl }}'
  },
  {
    name: COMPOSE_HTTP_TIMEOUT,
    value: '{{ .Values.env.composeHttpTimeout }}'
  }
]
{{- end -}}

{{- define "data-celery.vars" -}}
[
  # From Values:
  {
    name: PYTHONUNBUFFERED,
    value: '{{ .Values.env.pythonUnbuffered }}'
  },
  {
    name: SQLALCHEMY_DATABASE_URI,
    value: '{{ .Values.env.sqlAlchemyDatabaseUri }}'
  },
  {
    name: CELERY_BROKER_URL,
    value: '{{ .Values.env.celeryBrokerUrl }}'
  },
  {
    name: EXAPI_URL,
    value: '{{ .Values.env.exapiUrl }}'
  }, 
  {
    name: COMPOSE_HTTP_TIMEOUT,
    value: '{{ .Values.env.composeHttpTimeout }}'
  }
]
{{- end -}}