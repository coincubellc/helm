{{- define "celery-cron-job.vars" -}}
[
  # From Values:
  {
    name: PYTHONUNBUFFERED,
    value: '{{ .Values.env.pythonUnbuffered }}'
  },
  {
    name: SQLALCHEMY_DATABASE_URI,
    value: '{{ .Values.env.sqlAlchemyDatabaseUri }}'
  },
  {
    name: CELERY_BROKER_URL,
    value: '{{ .Values.env.celeryBrokerUrl }}'
  },
  {
    name: EXAPI_URL,
    value: '{{ .Values.env.exapiUrl }}'
  },
  {
    name: EMAIL_URL,
    value: '{{ .Values.env.emailUrl }}'
  },
  {
    name: COMPOSE_HTTP_TIMEOUT,
    value: '{{ .Values.env.composeHttpTimeout }}'
  },
  {
    name: VAULT_URL,
    value: '{{ .Values.env.vaultUrl }}'
  },

  # From secrets:
  {
    name: VAULT_TOKEN,
    valueFrom: {
      secretKeyRef: { name: coincube-secrets, key: vaultToken}
    }
  }  
]
{{- end -}}

{{- define "celery-scheduler.vars" -}}
[
  # From Values:
  {
    name: PYTHONUNBUFFERED,
    value: '{{ .Values.env.pythonUnbuffered }}'
  },
  {
    name: SQLALCHEMY_DATABASE_URI,
    value: '{{ .Values.env.sqlAlchemyDatabaseUri }}'
  },
  {
    name: CELERY_BROKER_URL,
    value: '{{ .Values.env.celeryBrokerUrl }}'
  },
  {
    name: EXAPI_URL,
    value: '{{ .Values.env.exapiUrl }}'
  },
  {
    name: EMAIL_URL,
    value: '{{ .Values.env.emailUrl }}'
  },
  {
    name: COMPOSE_HTTP_TIMEOUT,
    value: '{{ .Values.env.composeHttpTimeout }}'
  },
  {
    name: VAULT_URL,
    value: '{{ .Values.env.vaultUrl }}'
  },

  # From secrets:
  {
    name: VAULT_TOKEN,
    valueFrom: {
      secretKeyRef: { name: coincube-secrets, key: vaultToken}
    }
  }  
]
{{- end -}}