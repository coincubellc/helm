{{- define "coincube-front.vars" -}}
[
  # From Values:
  {
    name: APISERVER,
    value: '{{ .Values.env.apiServer }}'
  }
]
{{- end -}}