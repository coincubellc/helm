# Helm

Helm is a tool for managing Kubernetes charts. Charts are packages of pre-configured Kubernetes resources.

Helm is a tool that streamlines installing and managing Kubernetes applications. Think of it like apt/yum/homebrew for Kubernetes.

 - Helm has two parts: a client (`helm`) and a server (`tiller`) 
 - Tiller runs inside of your Kubernetes cluster, and manages releases (installations) of your charts. 
 - Helm runs on your laptop, CI/CD, or wherever you want it to run. 
 - Charts are Helm packages that contain at least two things: 
    - A description of the package (`Chart.yaml`) 
    - One or more templates, which contain Kubernetes manifest files
- Charts can be stored on disk, or fetched from remote chart repositories (like Debian or RedHat packages)

## Install

We are currently using helm `v2.9.1` install it using home-brew or your O.S package manager



## Runbooks

#### How to setup helm in your computer

First you need to have kubectl installed in your computer and also have a [kubeconfig file](https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/) for the cluster you want to access.

To access k8s prod cluster:
`export KUBECONFIG=~/.kube/config-prod`
To access k8s dev cluster:
`export KUBECONFIG=~/.kube/config-dev`

After configuring your kubeconfig file you need to configure helm in your computer:

```bash
$ helm init --client-only
$ $HELM_HOME has been configured at /Users/youruser/.helm.
Not installing Tiller due to 'client-only' flag having been set
Happy Helming!
```

To test if helm is working properly 

```bash
$ helm ls
NAME           	REVISION	UPDATED                 	STATUS  	CHART                      	NAMESPACE
prometheus     	3       	Thu Jun 14 17:43:56 2018	DEPLOYED	prometheus-6.7.0           	default
sentry         	5       	Mon Jun 11 21:06:22 2018	DEPLOYED	sentry-0.1.9               	default
```

If you have deployed charts in your cluster you should see something like that, if not you will have an empty list



#### How to deploy a new chart / upgrade chart / deploy a new docker image tag

Our charts are located at ` git@gitlab.com:coincubellc/helm.git`

You will allways work with helm on the repos root

```bash
$ cd helm/
```

Chart's config files are placed in `conf/$ENV/` folder that means we have a config file for every environment.



##### Deploy a new chart / upgrade chart

```bash
$ helm upgrade --install $release-name -f conf/$ENV/$chart-name.yaml $chart-name-path
```

Release name should allways be the name of the folder so if you want to deploy coincube-back:

```bash
$ helm upgrade --install coincube-back -f conf/prd/coincube-back.yaml coincube-back/
                                                                        
# notice the / at the end of $chart-name-path that means we are using the relative path since we are located at te repo's root
```



##### Deploy a new docker image tag

In helm you can pass values using -f $config-file.yaml or you can pass values in the cli.
In this example i will deploy the tag `master.1c6faf1` of coincube-back repo to prod

```bash
$ helm upgrade --install coincube-back -f conf/prd/coincube-back.yaml --set 'image.tag=master.1c6faf1' coincube-back/
```

All our docker images are built automatically on every push to master by [concourse](https://concourse.develop.coincube.io/) (running on dev kubernetes cluster so you must be connected to dev's vpn).

You should ALWAYS deploy a tested image tag, that's why we have continuous integration on dev. Every push to master is deployed on dev, after a full test we can go to prod and deploy it manually with the steps above. If you want to check all the posible tags you can go to the repo name on [AWS console](https://console.aws.amazon.com/ecs/home?region=us-east-1#/repositories)



#### How to add a new env variable to a chart

Let's say you want to add a new environment variable to a container, in this example we will use `email` chart.

1. Open the template `email/templates/_vars.tpl` in a text editor

2. Add a new value to the list:

   1.  Helm uses golang's templating format so the `value` key must be set with something like  `'{{ .Values.env.nameOfTheVariableUsingCamelCase }}'` 

      .Values --> because our conf/env/chart-name.yaml replaces helm's values.yaml file

      .env --> is the node where you are looking for the variable

      .nameOfTheVariableUsingCamelCase --> is the key where you are setting the real value, we use camel case for variable names with multiple words, for example the env variable `FLASK_DEBUG` on the template will be called `flaskDebug`

   2. Set the new variable in `conf/$env/$email.yaml` under the `env:` node

For example we want to add the environment variable `DEBUG`:

```bash
$ vim email/templates/_vars.tpl
{{- define "email.vars" -}}
[
  # From Values:
  {
    name: HOST,
    value: '0.0.0.0'
  },
  {
    name: PORT,
    value: '{{ .Values.service.port }}'
  },
  {
    name: DEBUG, # Real name of the ENV variable in the docker container
    value: '{{ .Values.env.debug }}' # Helms variable name and path
  }
]
{{- end -}}
```

Now we are going to set the value for dev and prod environment

```bash
$ vim conf/dev/email.yaml
# This file replaces the default's helm values.yaml

replicaCount: 1

image:
  repository: 648212798771.dkr.ecr.us-east-1.amazonaws.com/email
  tag       : latest
  pullPolicy: Always
  
+env:
+  debug: 'true'
+
service:
  type: ClusterIP
  port: 9090

  annotations:
    prometheus.io/probe: "true"

ingress:
  enabled: true
  path   : /
  hosts: [ "email.develop.coincube.io" ]
  annotations:
    dns.alpha.kubernetes.io/internal    : email.develop.coincube.io
    zalando.org/aws-load-balancer-scheme: external 
  
```

*Note: the lines with the + are the ones i added*

That's it, now to apply your changes [see](#deploy-a-new-chart-upgrade-chart)

