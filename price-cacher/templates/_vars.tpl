{{- define "price-cacher.vars" -}}
[
  # From Values:
  {
    name: PYTHONUNBUFFERED,
    value: '{{ .Values.env.pythonUnbuffered }}'
  },
  {
    name: SQLALCHEMY_DATABASE_URI,
    value: '{{ .Values.env.sqlAlchemyDatabaseUri }}'
  },
  {
    name: CRYPTOBAL_URL,
    value: '{{ .Values.env.cryptobalUrl }}'
  },
  {
    name: EXAPI_URL,
    value: '{{ .Values.env.exapiUrl }}'
  },
  {
    name: JDK_HOME,
    value: '{{ .Values.env.jdkHome }}'
  },
  {
    name: JAVA_HOME,
    value: '{{ .Values.env.javaHome }}'
  }
]
{{- end -}}