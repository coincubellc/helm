{{- define "coincube-back.vars" -}}
[
  # From Values:
  {
    name: PYTHONUNBUFFERED,
    value: '{{ .Values.env.pythonUnbuffered }}'
  },
  {
    name: SQLALCHEMY_DATABASE_URI,
    value: '{{ .Values.env.sqlAlchemyDatabaseUri }}'
  },
  {
    name: CRYPTOBAL_URL,
    value: '{{ .Values.env.cryptobalUrl }}'
  },
  {
    name: PRICE_CACHER_URL,
    value: '{{ .Values.env.priceCacherUrl }}'
  },
  {
    name: EXAPI_URL,
    value: '{{ .Values.env.exapiUrl }}'
  },
  {
    name: EMAIL_URL,
    value: '{{ .Values.env.emailUrl }}'
  },
  {
    name: FRONT_END_URL,
    value: '{{ .Values.env.frontEndUrl }}'
  },
  {
    name: DEVELOPMENT,
    value: '{{ .Values.env.development }}'
  },
  {
    name: FLASK_DEBUG,
    value: '{{ .Values.env.flaskDebug }}'
  },
  {
    name: PORT,
    value: '{{ .Values.service.port }}'
  },
  {
    name: HOST,
    value: '{{ .Values.env.host }}'
  },
  {
    name: JDK_HOME,
    value: '{{ .Values.env.jdkHome }}'
  },
  {
    name: JAVA_HOME,
    value: '{{ .Values.env.javaHome }}'
  },
  {
    name: VAULT_URL,
    value: '{{ .Values.env.vaultUrl }}'
  },

  # From secrets:
  {
    name: VAULT_TOKEN,
    valueFrom: {
      secretKeyRef: { name: coincube-secrets, key: vaultToken}
    }
  }  
]
{{- end -}}