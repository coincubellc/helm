{{- define "scheduler.vars" -}}
[
  # From Values:
  {
    name: PYTHONUNBUFFERED,
    value: '{{ .Values.env.pythonUnbuffered }}'
  },
  {
    name: SQLALCHEMY_DATABASE_URI,
    value: '{{ .Values.env.sqlAlchemyDatabaseUri }}'
  },
  {
    name: CELERY_BROKER_URL,
    value: '{{ .Values.env.celeryBrokerUrl }}'
  },
  {
    name: JDK_HOME,
    value: '{{ .Values.env.jdkHome }}'
  },
  {
    name: JAVA_HOME,
    value: '{{ .Values.env.javaHome }}'
  },
  {
    name: COMPOSE_HTTP_TIMEOUT,
    value: '{{ .Values.env.composeHttpTimeout }}'
  }
]
{{- end -}}

{{- define "trader-celery.vars" -}}
[
  # From Values:
  {
    name: PYTHONUNBUFFERED,
    value: '{{ .Values.env.pythonUnbuffered }}'
  },
  {
    name: SQLALCHEMY_DATABASE_URI,
    value: '{{ .Values.env.sqlAlchemyDatabaseUri }}'
  },
  {
    name: CELERY_BROKER_URL,
    value: '{{ .Values.env.celeryBrokerUrl }}'
  },
  {
    name: JDK_HOME,
    value: '{{ .Values.env.jdkHome }}'
  },
  {
    name: JAVA_HOME,
    value: '{{ .Values.env.javaHome }}'
  },
  {
    name: COMPOSE_HTTP_TIMEOUT,
    value: '{{ .Values.env.composeHttpTimeout }}'
  }
]
{{- end -}}