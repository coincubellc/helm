{{- define "email.vars" -}}
[
  # From Values:
  {
    name: HOST,
    value: '0.0.0.0'
  },
  {
    name: PORT,
    value: '{{ .Values.service.port }}'
  }
]
{{- end -}}